﻿using System;
using System.Runtime.InteropServices;

namespace StandAloneTest {
    class Program {
        static ulong Fib(ulong nth) {
            ulong pp, p = 0, c = 1;
            for (ulong i = 1; i < nth; i++) {
                pp = p;
                p = c;
                c = pp + p;
            }

            return c;
        }
        
        
        static void Main(String[] args) {
            System.Console.WriteLine("Please Enter Nth fib!");
            ulong nth;
            try {
                 nth = Convert.ToUInt16(System.Console.In.ReadLine());
            }
            catch (Exception) {
                System.Console.WriteLine("Error: {0}", "Please enter a valid unsigned 64 bit integer");
                return;
            }

            System.Console.WriteLine("The {0}th fib is:", nth);
            System.Console.WriteLine(Fib(nth));
        }
    }
}